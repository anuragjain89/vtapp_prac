require_relative './track_manager.rb'
class UserManager

  @user_bucket = {}


  def self.enroll_user_to_VTAPP(user)
    @user_bucket[user] = false
  end

  def self.activate_user(user)
    raise ArgumentError, 'User not found !' unless @user_bucket.has_key?(user)
    @user_bucket[user] = true
  end

  def self.deactivate_user(user)
    raise ArgumentError, 'User not found !' unless @user_bucket.has_key?(user)
    @user_bucket[user] = false
  end

  def self.get_all_user_array
    @user_bucket.keys
  end

  def self.get_active_user_array
    @user_bucket.select{ |k,v| v }.keys
  end

  def self.get_inactive_user_array
    @user_bucket.reject{ |k,v| v }.keys
  end

  def self.assign_track_to_user(user, track, validate_flag)
    if validate_flag
      raise ArgumentError, 'User not found !' unless @user_bucket.has_key?(user)
      raise ArgumentError, 'User in not active!' unless @user_bucket[user]
    end
    TrackManager.validate_track(track)
    user.assign_new_track(track)
  end

end

# p self.get_all_user_array