require_relative './task_bucket_loader.rb'
class Track

  extend TaskBucketLoader

  # TRACK_FILE_PATH_PREFIX = './tracks/'

  attr_accessor :name
  attr_reader :tasks

  def initialize(name)
    @name = name
    @tasks = get_task_bucket(name)
  end

  def get_task_bucket(name)
    file_name = "#{ name }.csv"
    TaskBucketLoader::load_task_bucket_from_file(file_name)
  end

  def ==(other)
    self.class === other and
    other.name == @name
  end

  alias eql? ==

  def started?
    @tasks.values.any? { |task| task.started }
  end

  def started_on
    tasks_started = @tasks.values.select { |task| task.started  }
    tasks_started.sort { |task| task.started_on }.first
  end

  def add_new_task(new_task)
    new_task.serial_number = @tasks.length + 1
    validate_task(new_task)
    @tasks[new_task.name] = new_task
  end

  def validate_task(new_task)
    raise ArgumentError, 'The argument is not a task' unless new_task.is_a? Task
  end

end
