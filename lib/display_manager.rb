require 'terminal-table'
require_relative './track.rb'
require_relative './user_bucket_loader'
class DisplayManager

  SCREEN_WIDTH = 100

  def self.display_screen(menu_name)
    clear_screen
    display_header(menu_name)
    print_line
    display_main_content(menu_name)
    print_line
    display_footer
  end

  def self.display_header(menu_name)
    print menu_name.upcase.center(SCREEN_WIDTH, ' ')
  end

  def self.display_footer

  end

  def self.display_main_content(menu_name)

  end

  def self.display_track_list(track_list)
    puts track_list
    print_line
    puts "Total number of tracks = #{track_list.length}"
  end

  def self.print_line
    puts '-' * SCREEN_WIDTH
  end

  def self.clear_screen
    print %x{clear}
  end

  def self.display_tasks_in_track(track)
    table = Terminal::Table.new do |t|
      track.get_task_bucket(track.name).each do |task_name, task|
        t << ["#{task.serial_number} => #{task.name}"]
        t.add_row [limit_string_to_length(task.description, SCREEN_WIDTH - 10)]
        t << :separator
        t.add_row ['Started', task.started ? 'Yes' : 'No']
        t.add_row ['Completed', task.completed ? 'Yes' : 'No']
        t.add_row ['Reviewed', task.started ? 'Yes' : 'No']
        t << :separator
        t.add_row ['']
      end
    end
    puts table
  end

  def self.limit_string_to_length(str, length)
    str.chars.each_slice(length).map(& :join).join("\n")
  end

  def self.display_user_summary(user)
    user.get_assigned_tracks.each do |track|
      puts track.name
      display_tasks_in_track(track)
    end
  end

end
