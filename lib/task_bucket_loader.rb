require 'csv'
require_relative './task.rb'

module TaskBucketLoader
  def self.load_task_bucket_from_file(file_name)
    file_name = File.expand_path("../tracks/#{file_name}",__FILE__)
    p file_name
    task_bucket = {}
    serial_number = 0
    file = CSV.foreach(file_name, :headers => true) do |task_row|
      task_name, task_description, task_type = task_row[0], task_row[1], task_row[2]
      new_task = Task.new(serial_number += 1, task_name, task_description, task_type)
      task_bucket[task_name] = new_task
    end
    task_bucket
  end
end
