class User

  attr_accessor :name, :email

  def initialize(name, email)
    @name = name
    @email = email
    @track_bucket = []
  end

  def ==(other)
    self.class  === other and
    other.name  == @name  and
    other.email == @email
  end

  alias eql? ==

  def hash
    @name.hash ^ @email.hash
  end

  def assign_new_track(track)
    raise ArgumentError, 'Track has already been assigned to this user' if @track_bucket.include?(track)
    @track_bucket.push(track)
  end

  def unassign_track(track)
    raise ArgumentError, 'Track has is not assigned to this user' unless @track_bucket.include?(track)
    @track_bucket.delete(track)
  end

  def get_assigned_tracks
    @track_bucket
  end

end