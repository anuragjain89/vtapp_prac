require 'csv'
require_relative './user.rb'
require_relative './track.rb'

module UserBucketLoader

  def self.load_initial_user_bucket
    load_user_bucket_from_file(File.expand_path('../users.csv',__FILE__))
  end

  def self.load_user_bucket_from_file(file_name)
    p file_name
    user_bucket = []
    serial_number = 0
    file = CSV.foreach(file_name, :headers => true) do |user_row|
      user_name, user_email, user_tracks = user_row[0], user_row[1], user_row[2]
      new_user = User.new(user_name, user_email)
      user_tracks.split(';').each do |track_name|
      new_user.assign_new_track(Track.new(track_name))
      end
      user_bucket.push(new_user)
    end
    user_bucket
  end

end

# user_bucket = UserBucketLoader.load_initial_user_bucket
# p user_bucket