require 'date'

class Task

  DEFAULT_REVIEWER = 'Akhil'

  attr_accessor :name, :description, :type, :reviewer, :serial_number
  attr_reader :started_on, :completed_on, :started, :completed, :reviewed, :reviewed_on

  def initialize(serial_number, name, description, type)
    @serial_number = serial_number
    @name = name
    @description = description
    @type = type
    @started = false
    @completed = false
    @reviewed = false
  end

  def start
    @started = true
    @started_on = Date.today
  end

  def mark_as_complete
    @completed = true
    @completed_on = Date.today
    assign_for_review
  end

  def mark_as_reviewed
    @reviewed = true
    @reviewed_on = Date.today
  end

  def assign_for_review
    @reviewer = get_reviewer
  end

  def get_reviewer
    DEFAULT_REVIEWER
  end

end
