require_relative('../lib/display_manager')
require_relative('../lib/user_bucket_loader')
require_relative('../lib/track_manager')
require_relative('../lib/user_manager')
require_relative('../lib/track')


user_bucket = UserBucketLoader.load_initial_user_bucket
DisplayManager.display_user_summary(user_bucket[0])

arvind = User.new('Arvind','arvind@delhicm.org')
UserManager.enroll_user_to_VTAPP(arvind)
UserManager.activate_user(arvind)
Basic_ruby = Track.new('Basic Ruby')
UserManager.assign_track_to_user(arvind, Basic_ruby, true)

p TrackManager.get_tracks_list
